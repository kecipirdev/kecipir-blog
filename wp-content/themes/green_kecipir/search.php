<?php get_header(); ?>

	<!-- Post -->
	<section class="bg0 p-b-55">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-12 p-b-80">

					<?php
						$s=get_search_query();
						$args = array('s' =>$s );


						_e('<div class="container p-t-40 p-b-40">
							<div class="row">
								<h2 class="f1-l-1 cl2">
									Hasil Pencarian : <i>'.get_query_var('s').'</i>
								</h2>
							</div>
						</div>'); ?>

					<div class="row">

						<?php

              // The Query
              $the_query = new WP_Query( $args );
              if ( $the_query->have_posts() ) {
              while ( $the_query->have_posts() ) { $the_query->the_post();
            ?>

  						<div class="col-sm-4 p-r-25 p-r-15-sr991">
  							<!-- Item latest -->
  							<div class="m-b-45">
                  <a href="<?php the_permalink(); ?>" class="wrap-pic-w hov1 trans-03">
                    <div class="bg-img1 size-a-5 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">

                    </div>
                  </a>

                  <div class="p-t-16">
                    <h5 class="p-b-5">
                      <a href="<?php the_permalink(); ?>" class="f1-m-2 cl2 hov-cl10 trans-03">
                        <?php the_title(); ?>
                      </a>
                    </h5>

                    <span class="cl8">
                      <span class="f1-s-3">
                        <?php the_time( 'j F Y' ); ?>
                      </span>
                    </span>
                  </div>
  							</div>
  						</div>

            	<?php }
            }else{ ?>

							<div class="m-t-40">
								<h3 class="f1-m-4 cl2">Artikel Tidak Ditemukan</h3>
								<h7>Maaf, kata kunci yang Anda gunakan tidak sesuai dengan kriteria.</h7>
							</div>


            <?php } ?>

					</div>

				</div>

			</div>
		</div>
	</section>

<?php get_footer(); ?>
