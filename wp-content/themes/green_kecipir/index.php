<?php get_header(); ?>

<!-- Feature post -->
<section class="bg0">
  <div class="container container-hero">
    <div class="row m-rl--1">
      <div class="col-md-6 p-rl-1 p-b-2">

        <?php
        $args = array( 'posts_per_page' => 1, );
        $category = get_the_category();

        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

          <div class="bg-img1 size-a-3 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">
            <a href="<?php the_permalink(); ?>" class="dis-block how1-child1 trans-03"></a>

            <div class="flex-col-e-s s-full p-rl-25 p-tb-20">
              <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
                <?php echo $category[0]->cat_name; ?>
              </a>

              <h3 class="how1-child2 m-t-14 m-b-10">
                <a href="<?php the_permalink(); ?>" class="how-txt1 size-a-6 f1-l-1 cl0 hov-cl10 trans-03">
                  <?php the_title(); ?>
                </a>
              </h3>

              <span class="how1-child2">
                <span class="f1-s-3 cl11">
                  <?php the_time( 'j F Y' ); ?>
                </span>
              </span>
            </div>
          </div>

        <?php endforeach;
        wp_reset_postdata();?>
      </div>

      <div class="col-md-6 p-rl-1">
        <div class="row m-rl--1">
          <div class="td-big-grid-scroll">
            <?php
            $args = array( 'posts_per_page' => 1, 'offset'=> 2, );
            $category = get_the_category();

            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

            <div class="col-md-12 item-hero p-rl-1 p-b-2">
              <div class="bg-img1 size-a-4 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">
                <a href="<?php the_permalink(); ?>" class="dis-block how1-child1 trans-03"></a>

                <div class="flex-col-e-s s-full p-rl-25 p-tb-24">
                  <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
                    <?php echo $category[0]->cat_name; ?>
                  </a>

                  <h3 class="how1-child2 m-t-14">
                    <a href="<?php the_permalink(); ?>" class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
                      <?php the_title(); ?>
                    </a>
                  </h3>

                </div>
              </div>
            </div>

            <?php endforeach;
            wp_reset_postdata();?>


            <?php
            $args = array( 'posts_per_page' => 2, 'offset'=> 3, );

            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

              <div class="col-md-6 item-hero p-rl-1 p-b-2">
                <div class="bg-img1 size-a-5 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">
                  <a href="<?php the_permalink(); ?>" class="dis-block how1-child1 trans-03"></a>

                  <div class="flex-col-e-s s-full p-rl-25 p-tb-20">
                    <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
                      <?php echo $category[0]->cat_name; ?>
                    </a>

                    <h3 class="how1-child2 m-t-14">
                      <a href="<?php the_permalink(); ?>" class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
                        <?php the_title(); ?>
                      </a>
                    </h3>
                  </div>
                </div>
              </div>

            <?php endforeach;
            wp_reset_postdata();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Post -->
<section class="bg0 p-t-70">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 col-lg-12">
        <div class="p-b-20">

          <div class="tab01 p-b-20">
            <div class="tab01-head how2 how2-cl4 bocl12 flex-s-c ">
              <!-- Brand tab -->
              <h3 class="f1-m-7 cl3 tab01-title bold">
                News
              </h3>

              <!--  -->
              <a href="<?php echo esc_url(home_url('news')); ?>" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
                View all
                <i class="fs-12 m-l-5 fa fa-caret-right"></i>
              </a>
            </div>


            <div class="p-t-35">
              <div class="row">
                <div class="col-sm-6 p-r-25 p-r-15-sr991">

                  <?php
                  $args = array( 'posts_per_page' => 1, 'category' => 32 );
                  $category = get_the_category();

                  $myposts = get_posts( $args );
                  foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                  <!-- Item post -->
                  <div class="m-b-30">
                    <a href="<?php the_permalink(); ?>" class="wrap-pic-w hov1 trans-03">
                      <img src="<?php the_post_thumbnail_url(); ?>" alt="IMG">
                    </a>

                    <div class="p-t-20">
                      <h5 class="p-b-5">
                        <a href="<?php the_permalink(); ?>" class="f1-m-2 cl2 hov-cl10 trans-03">
                          <?php the_title(); ?>
                        </a>
                      </h5>

                      <span class="cl8">
                        <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="f1-s-4 cl8 hov-cl10 trans-03">
                          <?php echo $category[0]->cat_name; ?>
                        </a>

                        <span class="f1-s-3 m-rl-3">
                          -
                        </span>

                        <span class="f1-s-3">
                          <?php the_time( 'j F Y' ); ?>
                        </span>
                      </span>
                    </div>
                  </div>
                  <?php endforeach;
                  wp_reset_postdata();?>
                </div>

                <div class="col-sm-6 p-r-25 p-r-15-sr991">

                  <?php
                  $args = array( 'posts_per_page' => 4, 'offset'=> 1, 'category' => 32 );
                  $category = get_the_category();

                  $myposts = get_posts( $args );
                  foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                    <!-- Item post -->
                    <div class="flex-wr-sb-s m-b-30">
                      <a href="<?php the_permalink(); ?>" class="size-w-1 wrap-pic-w hov1 trans-03">
                        <div class="bg-img1 size-a-9 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">

    	                  </div>
                      </a>

                      <div class="size-w-2">
                        <h5 class="p-b-5">
                          <a href="<?php the_permalink(); ?>" class="f1-s-5 cl3 hov-cl10 trans-03">
                            <?php the_title(); ?>
                          </a>
                        </h5>

                        <span class="cl8">
                          <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="f1-s-6 cl8 hov-cl10 trans-03">
                            <?php echo $category[0]->cat_name; ?>
                          </a>

                          <span class="f1-s-3 m-rl-3">
                            -
                          </span>

                          <span class="f1-s-3">
                            <?php the_time( 'j F Y' ); ?>
                          </span>
                        </span>
                      </div>
                    </div>
                  <?php endforeach;
                  wp_reset_postdata();?>


                </div>
              </div>
            </div>

          </div>

        </div>
      </div>


    </div>
  </div>
</section>



<?php

$categories = get_categories();

foreach ( $categories as $category ) {
  $args = array(
      'cat' => $category->term_id,
      'post_type' => 'post',
      'posts_per_page' => '6',
      'category__not_in' => array(1, 32, 299) //Lain-lain, News, Info dan Promo
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) { ?>

    <!-- Latest <?php echo $category->name; ?> -->
    <section id="post-<?php the_ID(); ?>" class="bg0 p-tb-25">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 col-lg-12 p-b-20">
            <div class="how2 how2-cl4 flex-s-c ">
              <h3 class="f1-m-7 cl3 tab01-title bold">
                <?php echo $category->name; ?>
              </h3>


              <!--  -->
              <a href="<?php echo get_category_link($category->cat_ID); ?>" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
                View all
                <i class="fs-12 m-l-5 fa fa-caret-right"></i>
              </a>
            </div>
            <div class="row-1">
              <div class="p-t-35 slider-item">

              <?php while ( $query->have_posts() ) { $query->the_post(); ?>

                <div class="item-s">
                  <!-- Item latest -->
                  <div class="m-b-45">
                    <a href="<?php the_permalink(); ?>" class="wrap-pic-w hov1 trans-03">
                      <div class="bg-img1 size-a-5 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">

                      </div>
                    </a>

                    <div class="p-t-16">
                      <h5 class="p-b-5">
                        <a href="<?php the_permalink(); ?>" class="f1-m-2 cl2 hov-cl10 trans-03">
                          <?php the_title(); ?>
                        </a>
                      </h5>

                      <span class="cl8">
                        <span class="f1-s-3">
                          <?php the_time( 'j F Y' ); ?>
                        </span>
                      </span>
                    </div>
                  </div>
                </div>

              <?php } // end while ?>

              </div>
            </div>
          </div>


        </div>
      </div>
    </section>

  <?php } // end if

  // Use reset to restore original query.
  wp_reset_postdata();
}
?>


<?php get_footer(); ?>
