<?php get_header(); ?>

  <!-- Breadcrumb -->
	<div class="container">
		<div class="headline bg0 flex-wr-sb-c p-tb-8">
			<div class="f2-s-1 p-r-30 m-tb-6">
				<?php custom_breadcrumbs(); ?>
			</div>
		</div>
	</div>

	<!-- Content -->
	<section class="bg0 p-b-140 p-t-10">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-30">
					<div class="p-r-10 p-r-0-sr991">

            <?php if(have_posts()) : while(have_posts())  : the_post();
              $category = get_the_category();
            ?>

              <!-- Blog Detail -->
  						<div class="p-b-70">
  							<a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="f1-s-10 cl2 hov-cl10 trans-03 text-uppercase">
  								<?php echo $category[0]->cat_name; ?>
  							</a>

  							<h3 class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
  								<?php the_title(); ?>
  							</h3>

  							<div class="flex-wr-s-s p-b-40">
  								<span class="f1-s-3 cl8 m-r-15">
  									<span>
                      <?php the_time( 'j F Y' ); ?>
  									</span>
  								</span>
  							</div>

  							<!-- <div class="wrap-pic-max-w p-b-30">
  								<?php echo the_post_thumbnail(); ?>
  							</div> -->

                <div class="content-inner">
                  <?php the_content(); ?>
                </div>

  						</div>

            <?php endwhile; endif; ?>

						<!-- Leave a comment -->
						<div class="comment-wrapper">
					    <div class="">
					      <p class="semibold uppercase">Comments</p>
					    </div>

					    <?php comments_template(); ?>
					  </div>

					</div>
				</div>

				<!-- Sidebar -->
				<div class="col-md-10 col-lg-4 p-b-30">
					<div class="p-l-10 p-rl-0-sr991 p-t-70">

						<!-- Category -->
						<div class="p-b-60">
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Category
								</h3>
							</div>

							<ul class="p-t-35">
								<?php
	                $categories =  get_categories();
	                foreach  ($categories as $category) { ?>
	                  <li class="how-bor3 p-rl-4">
	                    <a href="<?php echo get_category_link($category->cat_ID); ?>" class="dis-block f1-s-10 text-uppercase cl2 hov-cl10 trans-03 p-tb-13">
	                      <?php echo $category->cat_name; ?>
	                    </a>
	                  </li>
	              <?php } ?>
							</ul>
						</div>


						<!-- Popular Posts -->
						<div class="p-b-30">
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Popular Post
								</h3>
							</div>

							<ul class="p-t-35">
                <?php
  		          $args = array( 'posts_per_page' => 3 );
                $category = get_the_category();

  		          $myposts = get_posts( $args );
  		          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>


                  <li class="flex-wr-sb-s p-b-30">
                    <a href="<?php the_permalink(); ?>" class="size-w-10 wrap-pic-w hov1 trans-03">
                      <div class="bg-img1 size-a-9 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">

                      </div>
                    </a>


                    <div class="size-w-11">
                      <h6 class="p-b-4">
                        <a href="<?php the_permalink(); ?>" class="f1-s-5 cl3 hov-cl10 trans-03">
                          <?php the_title(); ?>
                        </a>
                      </h6>

                      <span class="cl8 txt-center p-b-24">
                        <a href="<?php echo get_category_link($category[0]->cat_ID); ?>" class="f1-s-6 cl8 hov-cl10 trans-03">
                          <?php echo $category[0]->cat_name; ?>
                        </a>

                        <span class="f1-s-3 m-rl-3">
                          -
                        </span>

                        <span class="f1-s-3">
                          <?php the_time( 'j F Y' ); ?>
                        </span>
                      </span>
                    </div>
                  </li>

  								<?php endforeach;
  			          wp_reset_postdata();?>


							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>


<?php get_footer(); ?>
