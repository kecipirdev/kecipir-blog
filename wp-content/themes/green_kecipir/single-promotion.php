<?php get_header(); ?>


	<!-- Content -->
	<section class="bg0 p-b-140 p-t-40">
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-30 promotion-box">
					<div class="">

            <?php if(have_posts()) : while(have_posts())  : the_post(); ?>

              <div class="row">
                <div class="col-md-12 wrap-pic-max-w thumb p-l-0 p-r-0">
  								<?php echo the_post_thumbnail(); ?>
  							</div>
              </div>

              <!-- Breadcrumb -->

              <div class="border-b row">
                <div class="col-md-12">
                  <div class="headline bg0 flex-wr-sb-c p-tb-8">
                    <div class="f2-s-1 p-r-30 m-tb-6">
                      <?php custom_breadcrumbs(); ?>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Blog Detail -->
  						<div class="promotion-detail">
  							<h3 class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
  								<?php the_title(); ?>
  							</h3>

                <div class="content-inner">
                  <?php the_content(); ?>
                </div>

  						</div>

            <?php endwhile; endif; ?>
					</div>
				</div>

				<!-- Sidebar -->
				<div class="col-md-10 col-lg-4 p-b-30 side-promo">
					<div class="p-l-10 p-rl-0-sr991">

						<!-- Category -->
						<div class="promotion-box">
              <div class="txt-center">
                <div class="col-md-12 p-all-20 border-b">
                  <h5 class="f1-m-7 cl2">
                    Info Promo
                  </h5>
                </div>
                <div class="col-md-12 border-b">
                  <div class="row">
                    <div class="<?php if (!empty(esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_minimum_transaksi', true ) ) )) { echo 'col-md-6'; }else{ echo 'col-md-12'; }  ?> p-all-10">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/ic_periode.png" alt="" class="img-icon-promo">
                      <div class="promotion-box-label text-secondary f1-s-6">Periode Promo</div>
                      <div class="promotion-box__value">
                           <?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_periode', true ) ); ?>
                      </div>
                    </div>
										<?php
											if (!empty(esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_minimum_transaksi', true ) ) )) { ?>
												<div class="col-md-6 p-all-10">
													<img src="<?php echo get_template_directory_uri(); ?>/images/ic_minimum.png" alt="" class="img-icon-promo">
													<div class="promotion-box-label text-secondary f1-s-6">Minimum Transaksi</div>
													<div class="promotion-box__value">
															 <?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_minimum_transaksi', true ) ); ?>
													</div>
		                    </div>
										<?php	} ?>

                  </div>
                </div>
                <div class="col-md-12 p-all-20">
                  <div class="col-md-12">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic_kode.png" alt="" class="img-icon-promo">
                    <div class="promotion-box-label text-secondary f1-s-6">Kode Promo</div>
                    <input type="text" id="promo-<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" type="text" class="sticky-code-voucher__text f1-m-4 cl2 copy txt-center float_none w-full m-b-10" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" readonly="">

                    <div class="col-md-12">
                      <a class="btn btn-ghost btn-small promotion-code__btn copied float_none" href="javascript:void(0);" id="copy-btn-<?php the_ID(); ?>" onclick="copyToClipboard('#promo-<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>')">Salin</a>
                    </div>
                  </div>
                </div>
              </div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</section>


  <script type="text/javascript">

       function copyToClipboardFF(text) {
            window.prompt("Copy to clipboard: Ctrl C, Enter", text);
        }

        function copyToClipboard(inputId) {
        var input = $(inputId);
            var success = true,
                    range = document.createRange(),
                    selection;
            // For IE.

            if (window.clipboardData) {
                window.clipboardData.setData("Text", input.val());
            } else {
                // Create a temporary element off screen.
                var tmpElem = $('<div>');
                tmpElem.css({
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px",
                });
                // Add the input value to the temp element.
                tmpElem.text(input.val());
                $("body").append(tmpElem);
                // Select temp element.
                range.selectNodeContents(tmpElem.get(0));
                selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
                // Lets copy.
                try {
                    success = document.execCommand("copy", false, null);
                }
                catch (e) {
                    copyToClipboardFF(input.val());
                }
                if (success) {
                    $('.copy.copyed').removeClass('copyed');
                    $('.btn-copyed').removeClass('copyed').removeClass('btn-copyed').text("Salin Kode");

                    $(input.selector).addClass('copyed');
                    $('#' + input.context.activeElement.id).text("Tersalain").addClass('btn-copyed');

                    tmpElem.remove();
                }
            }
        }


  </script>

<?php get_footer(); ?>
