<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">

		<link href="//www.google-analytics.coms" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php $title = wp_title("", false);?>
  	<title><?php echo ($title == '') ? get_bloginfo('name') : $title .' - '. get_bloginfo('name'); ?></title>
    <meta name="keywords"  content="" />
    <meta name="theme-color" content="#70bc52">

		<!-- favicon -->
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-114x114.png">
    <link rel="apple-touch-icon" sizes="228x228" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-228x228.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.png">

		<!-- google fontsl-->

    <link href="https://fonts.googleapis.com/css?family=Barlow:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  	<?php endif; ?>
  	<?php wp_head(); ?>

  </head>

<body <?php echo body_class();?>>

		<!-- Header -->
		<header>
			<!-- Header desktop -->
			<div class="container-menu-desktop">
				<div class="topbar">
					<div class="content-topbar container h-100">

						<?php if(is_singular( 'promotion' ) || is_post_type_archive('promotion')){ ?>
							<div class="left-topb">
								<a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_white_promo.png" alt="IMG-LOGO"></a>
							</div>
						<?php }else{ ?>
							<div class="left-topbar">
								<?php
									wp_nav_menu( array(
											'menu' => 'Top Menu'
									) );
								 ?>
							</div>
						<?php } ?>

						<div class="right-topbar">
							<a href="https://www.facebook.com/kecipir.organic/" target="_blank">
								<span class="fab fa-facebook-f"></span>
							</a>
							<a href="https://twitter.com/Kecipir_Segar" target="_blank">
								<span class="fab fa-twitter"></span>
							</a>
							<a href="https://www.instagram.com/kecipir/" target="_blank">
								<span class="fab fa-instagram"></span>
							</a>
						</div>
					</div>
				</div>

				<!-- Header Mobile -->
				<div class="wrap-header-mobile">
					<!-- Logo moblie -->
					<div class="logo-mobile">
						<a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="IMG-LOGO"></a>
					</div>

					<!-- Button show menu -->
					<div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</div>
				</div>

				<!-- Menu Mobile -->
				<div class="menu-mobile">
					<div class="main-menu-m">
						<?php
							wp_nav_menu( array(
									'menu' => 'Main Menu'
							) );
						 ?>

						 <div class="search-ov size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6">
							 <form class="search p-t-12 p-b-12" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
								 <input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="search" name="s" placeholder="<?php esc_attr_e( 'Search ..', 'nm-framework' ); ?>">
								 <button class="search-submit" type="submit" role="button"><i class="nm-font nm-font-search-alt"></i></button>
							 </form>

							 <button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
								 <i class="zmdi zmdi-search"></i>
							 </button>
						 </div>
					</div>


					<ul class="topbar-mobile">

						<li class="left-topbar">
							<?php
								wp_nav_menu( array(
										'menu' => 'Top Menu'
								) );
							 ?>
						</li>


						<li class="right-topbar">
							<a href="https://www.facebook.com/kecipir.organic/" target="_blank">
								<span class="fab fa-facebook-f"></span>
							</a>

							<a href="https://twitter.com/Kecipir_Segar" target="_blank">
								<span class="fab fa-twitter"></span>
							</a>

							<a href="https://www.instagram.com/kecipir/" target="_blank">
								<span class="fab fa-instagram"></span>
							</a>
						</li>




					</ul>
				</div>


				<?php if(!is_singular ( 'promotion' ) && !is_post_type_archive('promotion')){ ?>
					<!--  -->
					<div class="wrap-logo container">
						<!-- Logo desktop -->
						<div class="logo">
							<a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="LOGO"></a>
						</div>
					</div>
				<?php } ?>


				<!--  -->
				<div class="wrap-main-nav">
					<div class="main-nav">
						<!-- Menu desktop -->
						<nav class="menu-desktop">
							<a class="logo-stick" href="<?php echo esc_url(home_url()); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="LOGO">
							</a>

							<ul class="main-menu">
								<?php
									wp_nav_menu( array(
											'menu' => 'Main Menu',
											'container' => '',
											'items_wrap' => '%3$s'
									) );
								 ?>
							</ul>

							<a href="javascript:void(0);" class="search-btn">
								<i class="zmdi zmdi-search"></i>
							</a>


							<div class="search-ov size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6">
								<form class="search p-t-12 p-b-12" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
									<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="search" name="s" placeholder="<?php esc_attr_e( 'Search ..', 'nm-framework' ); ?>">
									<button class="search-submit" type="submit" role="button"><i class="nm-font nm-font-search-alt"></i></button>
								</form>

								<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
									<i class="zmdi zmdi-search"></i>
								</button>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
