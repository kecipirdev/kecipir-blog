
(function ($) {

    $('.textwidget.custom-html-widget').contents().unwrap();


    "use strict";

    /*==================================================================
    [ Load page ]*/
    try {
        $(".animsition").animsition({
            inClass: 'fade-in',
            outClass: 'fade-out',
            inDuration: 1500,
            outDuration: 800,
            linkElement: '.animsition-link',
            loading: true,
            loadingParentElement: 'html',
            loadingClass: 'animsition-loading-1',
            loadingInner: '<div class="loader05"></div>',
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: [ 'animation-duration', '-webkit-animation-duration'],
            overlay : false,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'html',
            transition: function(url){ window.location.href = url; }
        });
    } catch(er) {console.log(er);}



    if ($('.slider-item .slick-slide').length > 2 ) {

      $('.slider-item').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left'></i></button>",
        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right'></i></button>",
        responsive: [
         {
           breakpoint: 600,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 2
           }
         },
         {
           breakpoint: 400,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1
           }
         }
       ]


      });
    }



    $(".search-btn").click(function(e) {
      $(".search-ov").toggleClass('show');
    });

    /*==================================================================
    [ Back to top ]*/
    try {
        var windowH = $(window).height()/2;

        $(window).on('scroll',function(){
            if ($(this).scrollTop() > windowH) {
                $("#myBtn").addClass('show-btn-back-to-top');
            } else {
                $("#myBtn").removeClass('show-btn-back-to-top');
            }
        });

        $('#myBtn').on("click", function(){
            $('html, body').animate({scrollTop: 0}, 300);
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Fixed menu ]*/
    try {
        var posNav = $('.wrap-main-nav').offset().top;
        var menuDesktop = $('.container-menu-desktop');
        var mainNav = $('.main-nav');
        var lastScrollTop = 0;
        var st = 0;

        $(window).on('scroll',function(){
            fixedHeader();
        });

        $(window).on('resize',function(){
            fixedHeader();
        });

        $(window).on('load',function(){
            fixedHeader();
        });

        var fixedHeader = function() {
            st = $(window).scrollTop();

            if(st > posNav + mainNav.outerHeight()) {
                $(menuDesktop).addClass('fix-menu-desktop');
            }
            else if(st <= posNav) {
                $(menuDesktop).removeClass('fix-menu-desktop');
            }

            if (st > lastScrollTop){
                $(mainNav).removeClass('show-main-nav');
            }
            else {
                $(mainNav).addClass('show-main-nav');
            }

            lastScrollTop = st;
        };

    } catch(er) {console.log(er);}

    /*==================================================================
    [ Menu mobile ]*/
    try {
        $('.btn-show-menu-mobile').on('click', function(){
            $(this).toggleClass('is-active');
            $('.menu-mobile').slideToggle();
        });

        var arrowMainMenu = $('.arrow-main-menu-m');

        for(var i=0; i<arrowMainMenu.length; i++){
            $(arrowMainMenu[i]).on('click', function(){
                $(this).parent().find('.sub-menu-m').slideToggle();
                $(this).toggleClass('turn-arrow-main-menu-m');
            })
        }

        $(window).on('resize',function(){
            if($(window).width() >= 992){
                if($('.menu-mobile').css('display') === 'block') {
                    $('.menu-mobile').css('display','none');
                    $('.btn-show-menu-mobile').toggleClass('is-active');
                }

                $('.sub-menu-m').each(function(){
                    if($(this).css('display') === 'block') {
                        $(this).css('display','none');
                        $(arrowMainMenu).removeClass('turn-arrow-main-menu-m');
                    }
                });

            }
        });
    } catch(er) {console.log(er);}



      // Equal height
      // ====================
      function equal_height(){
        var height_array = $(".equal").map( function(){
          return  $(this).height();
        }).get();
        var max_height = Math.max.apply( Math, height_array);
        $(".equal").height(max_height);
      }

        equal_height();

      $(window).load(function() {
        equal_height();
      });

      $(window).resize(function(){
        equal_height();
      });


      $(document).ajaxComplete(function() {
        equal_height();
      });




})(jQuery);
