
	<div class="container p-t-4 txt-center p-b-40">
		<h6 class="f1-m-7 cl2 m-b-20">
			Unduh aplikasi <a href-"https://kecipir.com" target="_blank">kecipir.com</a> untuk belanja lebih mudah
		</h6>
		<ul class="download-apps">
			<li class="d-inline-table">
				<a href="https://apps.apple.com/us/app/kecipir/id1236623331?ls=1" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/app_store_badge.png" alt="">
				</a>
			</li>
			<li class="d-inline-table">
				<a href="https://play.google.com/store/apps/details?id=com.kecipir.kecipir" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/en-play-badge-border.png" alt="">
				</a>
			</li>
		</ul>
	</div>


	<!-- Footer -->
	<footer>
		<div class="bg2 p-t-40 p-b-25">
			<div class="container">
				<div class="row">
          <div class="col-sm-6 col-lg-4 p-b-20">
						<div class="f1-s-1 cl11 p-b-16">
							<a href="<?php echo esc_url(home_url()); ?>">
								<img class="logo-footer" src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.png" alt="LOGO">
							</a>
						</div>

						<div>
							<p class="f1-s-1 cl11 p-b-16">
                Kecipir adalah social enterprise untuk mewujudkan produksi, distribusi dan konsumsi pertanian secara lebih berkeadilan dan ramah lingkungan. Impian kami sederhana: Menjadikan sayuran organik itu menjadi "sayuran biasa" Dari sisi harga ia bisa bersaing, dari sisi pasokan ia bisa diandalkan, dari sisi konsumsi ia lebih sehat.
              </p>
              <img src="<?php echo get_template_directory_uri(); ?>/images/enviu_logo_grey.png" alt="LOGO">
						</div>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7">
								Popular Posts
							</h5>
						</div>

						<ul>
							<?php
		          $args = array( 'posts_per_page' => 3, 'offset'=> 1, );

		          $myposts = get_posts( $args );
		          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

								<li class="flex-wr-sb-s p-b-20">
									<a href="<?php the_permalink(); ?>" class="size-w-4 wrap-pic-w hov1 trans-03">
										<div class="bg-img1 size-a-9 how1 pos-relative" style="background-image: url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>);">

	                  </div>
									</a>

									<div class="size-w-5">
										<h6 class="p-b-5">
											<a href="<?php the_permalink(); ?>" class="f1-s-5 cl11 hov-cl10 trans-03">
												<?php the_title(); ?>
											</a>
										</h6>

										<span class="f1-s-3 cl6">
											<?php the_time( 'j F Y' ); ?>
										</span>
									</div>
								</li>

								<?php endforeach;
			          wp_reset_postdata();?>
						</ul>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7">
								Category
							</h5>
						</div>

						<ul class="m-t--12">
							<?php
								$categories =  get_categories();
								foreach  ($categories as $category) { ?>
									<li class="how-bor1 p-rl-5 p-tb-10">
										<a href="<?php echo get_category_link($category->cat_ID); ?>" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
											<?php echo $category->cat_name; ?>
										</a>
									</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="bg2">
			<div class="container size-h-4 p-tb-15">
				<span class="f1-s-1">
					<?php dynamic_sidebar( 'copyright' ); ?>
				</span>
			</div>
		</div>
	</footer>

	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>


<?php wp_footer(); ?>
</body>
<script>
jQuery( document ).ready(function() {
   

      jQuery('.slider-item').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left'></i></button>",
        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right'></i></button>",
        responsive: [
         {
           breakpoint: 600,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 2
           }
         },
         {
           breakpoint: 400,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1
           }
         }
       ]


      });
  
});
</script>
</html>
