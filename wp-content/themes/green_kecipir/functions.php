<?php


function no_generator() { return ''; }
add_filter( 'the_generator', 'no_generator' );


/**
 * Support Thumbnail
 */
add_theme_support( 'post-thumbnails' );

/**
 * Registers a widget area.
 */
function green_kecipir_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Copyright', 'green_kecipir' ),
		'id'            => 'copyright',
		'container' 		=> false,
		'before_widget' => '',
		'after_widget' => ''
	) );


	register_sidebar( array(
		'name'          => __( 'Banner Promo', 'green_kecipir' ),
		'id'            => 'banner_promo',
		'container' 		=> '',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
		'mime_type' => 'image',
	) );

}
add_action( 'widgets_init', 'green_kecipir_widgets_init' );


function get_dynamic_sidebar($i = 1) {
   $c = '';
   ob_start();
   dynamic_sidebar($i);
   $c = ob_get_clean();
   return $c;
}


/**
 * Style Login
 */
function my_custom_login() {
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');


/**
 * Register Menu
 */
register_nav_menus(
  array(
    'main_menu' => __( 'Main Menu', 'green_kecipir' ),
    'inner_main_menu' => __( 'Inner Main Menu', 'green_kecipir' ),
    'top_menu' => __( 'Top Menu', 'green_kecipir' )
  )
);


/**
 * Custom Admin Login
 */
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
  echo 'Kecipir &copy; 2020';
}

/*
 * Custom Login Logo
 */
add_action('login_head', 'green_kecipir_custom_login_logo');
function green_kecipir_custom_login_logo() {
	echo '<style type="text/css">
	.login h1 a { background-image: url('.get_bloginfo('template_directory').'/images/logo.png) !important; background-size:205px !important; height:85px !important; width:100% !important; }
	</style>';
}

add_filter( 'login_headerurl', 'green_kecipir_custom_login_url' );
function green_kecipir_custom_login_url($url) {
	return '';
}

/**
 * Custom Favicon
 */
add_action('admin_head','green_kecipir_favicon');
function green_kecipir_favicon(){
	echo '<link rel="shortcut icon" href="',get_template_directory_uri(),'/images/favicon/favicon.png" />',"\n";
}



// First, create a function that includes the path to your favicon
function add_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/images/favicon/favicon.png';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

// Now, just make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');


/**
 * Script
 */
function green_kecipir_scripts() {

	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '2.1.4');
	wp_enqueue_script( 'js-slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.0.0', TRUE );

	// wp_enqueue_script( 'js-global', get_template_directory_uri() . '/js/global.js', array('jquery'), '1.0.0', TRUE );
	wp_enqueue_script( 'js-animsition', get_template_directory_uri() . '/vendor/animsition/js/animsition.min.js', array('jquery'), '1.0.0', TRUE );
	wp_enqueue_script( 'js-popper', get_template_directory_uri() . '/vendor/bootstrap/js/popper.js', array('jquery'), '1.0.0', TRUE );
	wp_enqueue_script( 'js-bootstrap', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.min.js', array('jquery'), '1.0.0', TRUE );
	wp_enqueue_script( 'js-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.4.0', TRUE );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0' );
	wp_enqueue_style( 'css-bootstrap', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css', array(), '1.0' );
	wp_enqueue_style( 'css-fontawesome', get_template_directory_uri() . '/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css', array(), '1.0' );
	wp_enqueue_style( 'css-iconic', get_template_directory_uri() . '/fonts/iconic/css/material-design-iconic-font.min.css', array(), '1.0' );
	wp_enqueue_style( 'css-animate', get_template_directory_uri() . '/vendor/animate/animate.css', array(), '1.0' );
	wp_enqueue_style( 'css-css-hamburgers', get_template_directory_uri() . '/vendor/css-hamburgers/hamburgers.min.css', array(), '1.0' );
	wp_enqueue_style( 'css-animsition', get_template_directory_uri() . '/vendor/animsition/css/animsition.min.css', array(), '1.0' );
	wp_enqueue_style( 'css-util', get_template_directory_uri() . '/css/util.css', array(), '1.0' );
	wp_enqueue_style( 'css-main', get_template_directory_uri() . '/css/main.css', array(), '1.4' );
	wp_enqueue_style( 'css-slick-theme', get_template_directory_uri() . '/css/slick-theme.css', array(), '1.0' );
	wp_enqueue_style( 'css-slick', get_template_directory_uri() . '/css/slick.css', array(), '1.0' );
}

add_action( 'wp_enqueue_scripts', 'green_kecipir_scripts' );





function cptui_register_my_cpts_promotion() {

	/**
	 * Post Type: Promotion
	 */

	$labels = array(
		"name" => __( "Promotion", "green_kecipir" ),
		"singular_name" => __( "Promotion", "green_kecipir" ),
		"menu_name" => __( "Promotion", "green_kecipir" ),
		"all_items" => __( "All Promotion", "green_kecipir" ),
		"add_new" => __( "Add Promotion", "green_kecipir" ),
		"add_new_item" => __( "Add New Promotion", "green_kecipir" ),
		"edit_item" => __( "Edit Promotion", "green_kecipir" ),
		"new_item" => __( "New Promotion", "green_kecipir" ),
		"view_item" => __( "View Promotion", "green_kecipir" ),
		"view_items" => __( "View Promotion", "green_kecipir" ),
	);

	$args = array(
		"label" => __( "promotion", "green_kecipir" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => "promotion",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"show_in_admin_bar" => true,
    "menu_icon" => "dashicons-groups",
    "menu_position" => 4,
		"show_in_nav_menus" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "promotion", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail", "editor"),
	);

  register_post_type( "promotion", $args );
}
add_action( 'init', 'cptui_register_my_cpts_promotion' );

function promotion_init() {
		register_taxonomy(
				'promotion-category',
				'promotion',
				array(
						'label' => __( 'Promotion Cat' ),
						'rewrite' => array( 'slug' => 'promotion' ),
						'hierarchical' => true,
						'show_ui' => true,
		        'show_admin_column' => true,
		        'query_var' => true,
				)
		);
}
add_action( 'init', 'promotion_init' );






function opengraph_tags() {
    // defaults
    $title = get_bloginfo('title');
    $img_src = get_stylesheet_directory_uri() . '/images/logo.png';
    $excerpt = get_bloginfo('description');
    // for non posts/pages, like /blog, just use the current URL
    $permalink = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    if(is_single() || is_page()) {
        global $post;
        setup_postdata( $post );
        $title = get_the_title();
        $permalink = get_the_permalink();
        if (has_post_thumbnail($post->ID)) {
            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large')[0];
        }
        $excerpt = get_the_excerpt();
        if ($excerpt) {
            $excerpt = strip_tags($excerpt);
            $excerpt = str_replace("", "'", $excerpt);
        }
    }
    ?>

		<!-- meta tag facebook -->
		<meta name="resource-type" content="document" />
		<!-- <meta property="og:title" content="<?= $title; ?>"/>
		<meta property="og:description" content="<?= $excerpt; ?>"/> -->
		<meta property="og:url" content="<?= $permalink; ?>"/>
		<meta property="og:site_name" content="<?= get_bloginfo(); ?>"/>
		<meta property="og:type" content="<?php if ( is_front_page() ) { echo "website"; } else { echo "article"; } ?>">

		<meta name="language" content="Indonesia" />
		<meta name="organization" content="<?= get_bloginfo(); ?>" />
		<meta name="copyright" content="Copyright (c)2019 <?= get_bloginfo(); ?>" />
		<meta name="audience" content="<?= get_bloginfo(); ?>" />
		<meta name="classification" content="Indonesia, English, Company Profile, Company Spirit" />
		<meta name="rating" content="general" />
		<meta name="page-topic" content="" />
		<meta name="revisit-after" content="7 days" />
		<meta name="mssmarttagspreventparsing" content="true" />


	<?php if(!is_single()){
	if(is_home() || is_front_page()){ // not sure if you have set a static page as your front page
		echo '<meta property="og:url" content="'.get_bloginfo('url').'" />';
	}elseif(is_tag()){
		echo '<meta property="og:url" content="http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].' ">';
		}
	} ?>

<?php
}
add_action('wp_head', 'opengraph_tags', 5);



// Breadcrumbs
function custom_breadcrumbs() {

    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Homepage';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            }

            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

        } else if ( is_single() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';


            }

            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {

                // Get last category post is in
                $last_category = end(array_values($category));

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    // $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }

            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;

            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                // echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {

                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
            }

        } else if ( is_category() ) {

            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        } else if ( is_page() ) {

            // Standard page
            if( $post->post_parent ){

                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;

            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';

        } elseif ( is_404() ) {

            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }

        echo '</ul>';

    }

}


function fix_slash( $string, $type ) {
  global $wp_rewrite;
  if ( $wp_rewrite->use_trailing_slashes == false ) {
    if ( $type != 'single' && $type != 'category' )
      return trailingslashit( $string );

    if ( $type == 'single' && ( strpos( $string, '.html/' ) !== false ) )
      return trailingslashit( $string );

    if ( $type == 'category' && ( strpos( $string, 'category' ) !== false ) ){
      $aa_g = str_replace( "/category/", "/", $string );
      return trailingslashit( $aa_g );
    }
    if ( $type == 'category' )
      return trailingslashit( $string );
  }
  return $string;
}

add_filter( 'user_trailingslashit', 'fix_slash', 55, 2 );



/**
 * Register meta boxes.
 */
function hcf_register_meta_boxes() {
    add_meta_box( 'hcf-1', __( 'Promo Detail', 'hcf' ), 'hcf_display_callback', 'promotion' );
}
add_action( 'add_meta_boxes', 'hcf_register_meta_boxes' );



function hcf_display_callback( $post ) {
	?>

	<div class="hcf_box">
	    <style scoped>
	        .hcf_box{
	            display: grid;
	            grid-template-columns: max-content 1fr;
	            grid-row-gap: 10px;
	            grid-column-gap: 20px;
	        }
	        .hcf_field{
	            display: contents;
	        }
	    </style>

	    <p class="meta-options hcf_field">
	        <label for="hcf_kode_promo">Kode Promo</label>
	        <input id="hcf_kode_promo"
	            type="text"
	            name="hcf_kode_promo"
	            value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>">
	    </p>

	    <p class="meta-options hcf_field">
	        <label for="hcf_promo_periode">Promo Periode</label>
	        <input id="hcf_promo_periode"
	            type="text"
	            name="hcf_promo_periode"
	            value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_periode', true ) ); ?>">
	    </p>

	    <p class="meta-options hcf_field">
	        <label for="hcf_promo_minimum_transaksi">Minimum Transaksi</label>
	        <input id="hcf_promo_minimum_transaksi"
	            type="text"
	            name="hcf_promo_minimum_transaksi"
	            value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_minimum_transaksi', true ) ); ?>">
	    </p>
	</div>

<?php }


/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function hcf_save_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $fields = [
        'hcf_kode_promo',
        'hcf_promo_periode',
        'hcf_promo_minimum_transaksi'
    ];
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, sanitize_text_field( $_POST[$field] ) );
        }
     }
	}
	add_action( 'save_post', 'hcf_save_meta_box' );










	/**
	 * Pagination
	 */

	function pagination_numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";



    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }


    echo '</ul></div>' . "\n";

}




function custom_pre_get_posts( $query ) {
if( $query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {
    $query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );  }  }

add_action('pre_get_posts','custom_pre_get_posts');

function custom_request($query_string ) {
     if( isset( $query_string['page'] ) ) {
         if( ''!=$query_string['page'] ) {
             if( isset( $query_string['name'] ) ) { unset( $query_string['name'] ); } } } return $query_string; }

add_filter('request', 'custom_request');






//Search
function search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $query->set('post_type', array( 'product' ) );
    }
  }
}

add_action('pre_get_posts','search_filter');



add_filter('comment_form_default_fields', 'website_remove');
function website_remove($fields) {
	if(isset($fields['url']))
	unset($fields['url']);
	return $fields;
}





?>
