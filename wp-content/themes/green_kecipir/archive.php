<?php get_header(); ?>

<?php $the_query = new WP_Query( 'page_id=3301' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
  <section class="bg0 content-wrapper">
    <div class="banner-promo" style="background:url('<?php echo the_post_thumbnail_url(); ?>')"></div>

    <div class="container">
      <div class="size-a-400 bo-1-rad-22 of-hidden bocl11 m-tb-6 d_relative">
        <input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45 quicksearch" type="text" name="quicksearch" placeholder="Cari Promo" >
        <button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
          <i class="zmdi zmdi-search"></i>
        </button>
      </div>
    </div>
  </div>
<?php endwhile;?>


<!-- Post -->
<section class="bg0 p-t-30 p-b-55 ">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 col-lg-12 p-b-80">
        <div class="row grid d_relative">

          <?php if(have_posts()) : while(have_posts())  : the_post(); ?>

            <div class="promotion-item col-md-4 col-sm-6 p-r-25 p-r-15-sr991">
              <div class="promotion-box m-b-45">
                <a href="<?php the_permalink(); ?>" class="wrap-pic-w hov1 trans-03">
                  <img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else{ echo get_template_directory_uri().'/images/thumb-default.png'; } ?>" alt="IMG">
                </a>

                <div class="promotion-description">
                  <a href="<?php the_permalink(); ?>" class="f1-m-1 cl2 hov-cl10 trans-03 equal">
                    <?php the_title(); ?>
                  </a>
              	  <div class="promotion-date">
                		<div class="promotion-date-detail">
                		  <div class="promotion-box-label text-secondary">Periode Promo</div>
                  		  <div class="promotion-box__value">
                  			     <?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_promo_periode', true ) ); ?>
                        </div>
                		</div>
              	  </div>
              	  <div class="promotion-code">

                		<div class="promotion-code-detail">
                		  <div class="promotion-box-label text-secondary">
                			  Kode Promo
                		  </div>
                		  <input type="text" id="promo-<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" type="text" class="sticky-code-voucher__text f1-m-4 cl2 copy" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>" readonly="">
                		</div>
                		<a class="btn btn-ghost btn-small promotion-code__btn copied" href="javascript:void(0);" id="copy-btn-<?php the_ID(); ?>" onclick="copyToClipboard('#promo-<?php echo esc_attr( get_post_meta( get_the_ID(), 'hcf_kode_promo', true ) ); ?>')">Salin</a>

              	  </div>

              	</div>
                <div class="promotion-cta">
                  <a target="_blank" href="<?php the_permalink(); ?>" class="promotion__btn">Lihat Detail</a>
                </div>

              </div>
            </div>

          <?php endwhile; endif; ?>

        </div>

        <!-- Pagination -->
        <?php pagination_numeric_posts_nav(); ?>

      </div>
    </div>
  </div>
</section>

<script src='<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.js'></script>
<script src='<?php echo get_template_directory_uri(); ?>/js/imagesloaded.pkgd.js'></script>

<script type="text/javascript">

     function copyToClipboardFF(text) {
          window.prompt("Copy to clipboard: Ctrl C, Enter", text);
      }

      function copyToClipboard(inputId) {
      var input = $(inputId);
          var success = true,
                  range = document.createRange(),
                  selection;
          // For IE.

          if (window.clipboardData) {
              window.clipboardData.setData("Text", input.val());
          } else {
              // Create a temporary element off screen.
              var tmpElem = $('<div>');
              tmpElem.css({
                  position: "absolute",
                  left: "-1000px",
                  top: "-1000px",
              });
              // Add the input value to the temp element.
              tmpElem.text(input.val());
              $("body").append(tmpElem);
              // Select temp element.
              range.selectNodeContents(tmpElem.get(0));
              selection = window.getSelection();
              selection.removeAllRanges();
              selection.addRange(range);
              // Lets copy.
              try {
                  success = document.execCommand("copy", false, null);
              }
              catch (e) {
                  copyToClipboardFF(input.val());
              }
              if (success) {
                  $('.copy.copyed').removeClass('copyed');
                  $('.btn-copyed').removeClass('copyed').removeClass('btn-copyed').text("Salin Kode");

                  $(input.selector).addClass('copyed');
                  $('#' + input.context.activeElement.id).text("Tersalain").addClass('btn-copyed');

                  tmpElem.remove();
              }
          }
      }

      // quick search regex
      var qsRegex;

      // init Isotope
      var $grid = $('.grid').isotope({
        itemSelector: '.promotion-item',
        layoutMode: 'fitRows',
        filter: function() {
          return qsRegex ? $(this).text().match( qsRegex ) : true;
        }
      });

      // use value of search field to filter
      var $quicksearch = $('.quicksearch').keyup( debounce( function() {
        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
        $grid.isotope();
      }, 200 ) );

      // debounce so filtering doesn't happen every millisecond
      function debounce( fn, threshold ) {
        var timeout;
        threshold = threshold || 100;
        return function debounced() {
          clearTimeout( timeout );
          var args = arguments;
          var _this = this;
          function delayed() {
            fn.apply( _this, args );
          }
          timeout = setTimeout( delayed, threshold );
        };
      }

      /*=============Code for Masonry Gallery ==============*/
      var grid = document.querySelector('.grid');
      var jQuerygrid = jQuery('.grid').isotope({
          itemSelector: '.promotion-item'
      });
      var iso = jQuerygrid.data('isotope');
      jQuerygrid.isotope( 'reveal', iso.items );

      imagesLoaded(grid, function(){
          iso.layout();
      });



</script>

<?php get_footer(); ?>
