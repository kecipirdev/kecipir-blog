=== Page Visit Counter ===
Plugin Name: Page Visit Counter
Plugin URI: https://www.thedotstore.com/
Author: Thedotstore
Author URI: https://www.thedotstore.com/
Contributors: dots, ketuchetan, chiragpatel, jitendrabanjara1991, niravcse006
Stable tag: 6.0.2
Tags: page counter,page visit, post counter, post visit, wordpress post view, wordpress page view, page visit graph, post visit graph, 
Requires at least: 3.0
Tested up to: 5.2.2
Donate link:
Copyright: (c) 2014-2016 Multidots Solutions PVT LTD (info@multidots.com) 
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin will count the total visits of your sites pages.

== Description ==
Page visit counter plugin helps you to boost conversion and increase visitor engagement.

You can display a real-time page and post visitor counter on all page or specif page.

Page visit counter will help increase conversions by transfer visitor to customer, subscribers and  reducing bounce rates.

= How it Works =
Add a site visitor counter, decide its layout and create wizards for separate tracking of pages/posts using this comprehensive plugin.

It also enables site owners to view analytics related to multiple metrics and analyze the traffic intensely.

Plugin Demo : <a href ="http://pagevisitcounter.demo.store.multidots.com/" target="_blank">View Demo</a>

= Key features =

* Plugin used for front side post and pages visit counter.
* Front-end side, you can display individual page visit count as well as total website count to the bottom of the pages and posts on your WordPress website.
* Display page visit counter on page and post
* Enable / Disable Page Visit Count for all pages and posts.
* Set the background as well as font color to display a page visit counter on the website.
* You can exclude by IP, User, and User Role for page coun
* Page view with last month with top 10-page graph and all page list.
* Post view with last month with top 10 post graph and all post list.
* Search by post/page name in page/post list.
* Check individual page/post-visit history with its graph.
* Enable/Disable individual page visit count through meta box in page and post. Also, you can display today's page visit count
* Reset individual page count through meta box in page and post.
* You can add custom CSS.
* You can see the admin dashboard with a different type of report. As per below
* Website site visitors summary like today, yesterday, last week, last year, etc.
* Browse and operating system report
* Top 10 Country, Post, Page, Referral site and IP list based visitor reports

[youtube https://www.youtube.com/watch?v=PAF6SBy-PdY]

= Page Counter Settings: =
In these options, you can do different settings for the page visit counter.

The plugin provides a default setting for the page visit counter.

* Title: Enter the title which you want to be displayed in front side counter for the wizard.
* Post Type: -  You can select the post type from the drop-down menu for which post views will be counted. If you leave blank on post type, then all pages or all past type posts will be counted.
* Front View Counter: It will use to display all page/posts visit count on the front side without using any wizard/shortcode. By default it is Disable.
* Style: You can set different styles with font family, font size, font color, background color, border, border-radius, padding as per your requirement.
* Counter View & Position: You can set your page visit counter position as per your requirement on the front side. And also you can specify who is the display page visit counter on the front side. Also, you can manage the post-visit counter icon as per your requirement.
* Exclude IPs (Ip Address): Enter the IP addresses which you want to be excluded from post views count.
* Exclude Users: Select users from your project/system to be excluded from post view count.
* Exclude User Role: Select user role from your project/system to be excluded from post view count.
* Exclude Country: Select the country in which you want to be excluded from post view count.
* Custom CSS: You can add custom CSS as per your requirement.
* Short Code: Total Website Count - It will display the sum of all page/post count.
* Short Code: Individual Wizard - It will display individual page/post count.

== Pro Plugin Overview: ==
Need even more features? upgrade to <a href ="https://www.thedotstore.com/page-visit-counter/" target="_blank">Page Visit Counter PRO</a> to get all the advanced features:

* Multiple Create shortcode and wizard
* Visitors Counter is fully customizable. Visitors Counter is fully customizable.
* Exclude Page visit counter by User Roles
* Exclude Page visit counter by Country
* Counter view - Who will see this page visit counter  by Guest Only, Registered Users only
* Upload Counter Icon
* Monthwise Website Visit Counter Report
* Yearwise Website Visit Report
* Individual Page/Post Graph with 2 Month
* Individual Page/Post Graph with 3 Month
* Individual Page/Post Graph with 6 Month
* Page/Post summary with User Role
* Backend Individual wizard setting
* Backend Individual wizard Delete

= Example: =
1. Admin can see all pages counter in table format from admin site in "Page Visit Counter" option.
2. After Activation of plugin it will display page count in all pages  at bottom of your site. If you do not want to display or you want to exclude particular post type from page views count then at that time you exclude particular post type in "Post type" setting of page counter.
3. If you want that pages visit/hits from Specific IP not include in page counter then you can add that Specific id to "Exclude IPs (Ip Address)" on setting of page counter and then page request comes from that Specific IP is not included in page visit counter.
4. If you want that specific hits/visits of pages comes from  register users of your site that is  not include in page counter then you can add name of that Specific register user of your site on "Exclude Users" and then page request comes from that Specific register user is not included in page visit counter.
5. There are two shortcodes that you can use to manually add page view count to any content on admin or post/page template created by your theme or plugin thats create it's own display content in page/post.
   1. Use this shortcode to add admin side content on page/post: [pvcp_1]
   2. Use this shortcode to display total sites visit to add admin side content on page/post: [pvcp_website_count]
   3. Use this shortcode to add page/post template (.php) file of your own template: `<?php echo do_shortcode('[pvcp_1']');?>`
   4. Use this shortcode to display total sites visit to add page/post template (.php) file of your own template: `<?php echo do_shortcode('[pvcp_website_count]');?>`

= Note: =
In the migration process, all existing data converted into the page list. And after that new version provide page count with two different sections as Post(Blog) and Page. 

We always welcome user suggestions. Let us know what you think about this plugin you liked or may have disliked. User's feedback is important for us to improve more our plugins.

= You can check our other plugins: =
<ol>
<li> <a href ="http://bit.ly/2FrC4id">Emporos - Responsive WooCommerce Theme</a></li>
<li> <a href ="http://www.thedotstore.com/advanced-flat-rate-shipping-method-for-woocommerce">Advance Flat Rate Shipping Method For WooCommerce</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-blocker-lite-prevent-fake-orders-blacklist-fraud-customers/">WooCommerce Blocker – Prevent Fake Orders And Blacklist Fraud Customers</a></li>
<li> <a href ="http://www.thedotstore.com/woocommerce-enhanced-ecommerce-analytics-integration-with-conversion-tracking">WooCommerce Enhanced Ecommerce Analytics Integration With Conversion Tracking</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-category-banner-management/">Woocommerce Category Banner Management</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-conditional-product-fees-checkout/">Woocommerce Conditional Extra Fees</a></li>
<li> <a href ="https://www.thedotstore.com/woocommerce-advanced-product-size-charts/">Woocommerce Advanced Product Size Charts</a></li>
<li> <a href ="http://www.thedotstore.com/advance-menu-manager-wordpress/">Advance Menu Manager for WordPress</a></li>
<li> <a href ="http://www.thedotstore.com/woocommerce-save-for-later-cart-enhancement">Woocommerce Save For Later Cart Enhancement</a></li>
</ol>

== Installation ==

= Minimum Requirements =

* WordPress 3.0 or higher

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Page Visit Counter, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type Page Visit Counter and click Search Plugins. Once you've found our plugin you can view details about it such as the the rating and description. Most importantly, of course, you can install it by simply clicking Install Now?.

= Manual Installation =

1. Unzip the files and upload the folder into your plugins folder (/wp-content/plugins/) overwriting older versions if they exist
2. Activate the plugin in your WordPress admin area.

== Screenshots ==
1. Page Visit Counter Screenshot 1.
2. Page Visit Counter Screenshot 2.
3. Page Visit Counter Screenshot 3.
4. Page Visit Counter Screenshot 4.
5. Page Visit Counter Screenshot 5.
6. Page Visit Counter Screenshot 6.
7. Page Visit Counter Screenshot 7.
8. Page Visit Counter Screenshot 8.

== Frequently Asked Questions ==

In which WordPress version this Plugin is compatible?

It is compatible from 2.1 to 5.0.3 WordPress version.

== Upgrade Notice ==

Automatic updates should work great for you.  As always, though, we recommend backing up your site prior to making any updates just to be sure nothing goes wrong.

== Changelog ==

= 6.0.1 - 20-11-2019 =
* Compatible with Wordpress 5.3.x
* Compatible with WooCommerce 3.8.x

= 6.0.1  - 09-09-2019 =
* Fixed Migration Script which is display as a fatal error with stop execution.


= 6.0  - 05-09-2019 =
* Individual Wizard
* Change graph with website summary
* Enhanced Plugin UI
* Integrate with freemius
* Migration form old version to new


= 5.3  - 28-05-2019 =
* Compatible with WordPress Version 5.2.x and WooCommerce version 3.6.x
* Minor bug fixing

= 5.2  - 26-03-2019 =
* Border attribute not working issue fixing

= 5.1  - 26-03-2019 =
* minor bug fixed.

= 5.0  - 15-02-2019 =
* compatible with WordPress 5.0.x
* Compatible with PHP 7.2.0
* minor bug fixed.

= 4.9  - 21-12-2018 =
* compatible with WordPress 5.0.x

= 4.8  - 13-11-2018 =
* compatible with php 7.2.0 version.

= 4.7  - 10-08-2018 =
* minor bug fixed.

= 4.6  - 14-06-2018 =
* Fixed language issue.

= 4.5  - 11-06-2018 =
* Fix vulnerable plugin issue.

= 4.4  - 08-06-2018 =
* minor bug fixed.

= 4.3  - 06-06-2018 =
* Fixed vulnerable code issue
* Compatible with WordPress 4.9.x

= 4.2  - 06-06-2018 =
* minor bug fixed.

= 4.1  - 31-05-2018 =
* minor bug fixed.

= 4.0.9  - 29-05-2018 =
* minor bug fixed.

= 4.0.8  - 25-05-2018 =
* minor bug fixed.

= 4.0.7  - 11-05-2018 =
* minor bug fixed.

= 4.0.6  - 27-04-2018 =
* minor bug fixed.

= 4.0.5  - 11-04-2018 =
* Solved issue of view reports.
* minor bug fixing.
* Compatible with WordPress Version 4.9.4

= 4.0.4  - 19-06-2017 =
* Removed Total visit on share post title.
* Compatible with WordPress Version 4.87

= 4.0.3  - 24-12-2016 =
* Fixies - Fixed the shortcode issues.
* Compatible with WordPress Version 4.7

= 4.0.2  - 06-10-2016 =
* Fixies - Fixed the Globally count issue.
* Change the readme file.

= 4.0.1  - 03-10-2016 =
* Change the readme file.
* Added note on the admin plugin configuration

= 4.0  - 30-09-2016 =
* Check wordpress and woocommerce compatibility
* Fixies - Shop page and checkout page error handled
* Fixies - PHP error notice handled.
* Fixies - Social Sharing issue
* New - Added Shortcode to display total sites visit counts.
* New - Added today count display
* New - Admin Interface changes
* New - Added responsive for the backend

= 3.0.4  - 26-08-2016 =
* Check wordpress and woocommerce compatibility
* Fixies - Shop page and checkout page error handled

= 3.0.3 - 14.07.2016 =
* Fixies - PHP error notice handled.


= 3.0.2 - 11.04.2016 =
* Added reports like Top browsers, Top 10 IP address, Top referer, weekly report and Monthly report using chart
* Fixies - PHP error notice handled.
* Fixies - Jquery error handled.

= 3.0.1 - 06.04.2016 =
* Fixies - Removed report functionality in fancybox
* Notes: Reverted version 2.0.4

= 3.0.0 - 05.04.2016 =
* Fixies - PHP error notice handled.
* Fixies - sharing button on search results
* Added reports like Top browsers, Top 10 IP address, Top referer, weekly report and Monthly report using chart

= 2.0.4 - 17.03.2016 =
* Fixies - PHP error notice handled.
* New - Added social sharing on Facebbok, Twitter and Google plus for the admin in backend.
* New - Added color options to choose the color for display visit pages text on front side as well as in the shortcode.
* New - Added different 9(nine) languages all are supported wordpress.

= 2.0.3 - 03.03.2016 =
* Fixies - PHP error notice handled.
* Fixies - Set the global visible NO to display counter view on front end.

= 2.0.2 - 25.02.2016 =
* Fixies - PHP error notice handled.
* Fixies - Fixed the display issue on WooCommerce single product page.
* New - Added global option on plugin settings page to hide the total visits block fron front end.
* New - Added counter on the WooCommerce shop page.
* New - Added total visit count on single post/page edit view in admin side meta box.
* New - Added total visit count on single post/page listing view in admin side.

= 2.0.1 - 24.02.2016 =
* Fixies - PHP error notice handled.
* New - Remove counter for the homepage.

= 2.0.0 - 23.02.2016 =
* New - Exclude custom post type from Page / Post visit counter.
* New - Eclude Specific IP and Registered Users from Page / Post visit Counter.
* New - Display page visit count on bottom of page on entire site.

= 1.1.3 - 08.01.2016 =
* Fixes - PHP error notice handled.
* Fixes - SSL issue resolved.
* Fixes - Admin Menu Page issue resolved.
* New - Readme file added instructions.

= 1.1.2 - 14.12.2015 =
* Fixes - PHP error notice handled.

= 1.1.1 - 01.12.2015 =
* Tweak - Remote request handles on activate.

= 1.1 - 07.11.2015 =
* Tweak - Class file added for admin settings page output.